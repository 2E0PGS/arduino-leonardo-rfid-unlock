/*
Simple program for Unlocking Windows, Linux or OSX desktop via RFID Tags.

Copyright (C) <2016-2020>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno           Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS 1    SDA(SS)      10            53        D10        10               10
 * SPI SS 2    SDA(SS)      2             53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 *
 */

byte OurCardUID[07] = {0xA2, 0x9A, 0xC8, 0xB9, 0x00, 0x5F, 0x00};

#include <Keyboard.h>
#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         5           // Configurable, see typical pin layout above
#define SS_1_PIN        10          // Configurable, see typical pin layout above
#define SS_2_PIN        10          // Configurable, see typical pin layout above

#define NR_OF_READERS   2

byte ssPins[] = {SS_1_PIN, SS_2_PIN};

MFRC522 mfrc522[NR_OF_READERS];   // Create MFRC522 instance.

void setup() {

  Keyboard.begin(); // initialize control over the keyboard.
  Serial.begin(9600); // Initialize serial communications with the PC.
  SPI.begin(); // Init SPI bus.

  for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
    mfrc522[reader].PCD_Init(ssPins[reader], RST_PIN); // Init each MFRC522 card
  }

}

void loop() {

    for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
    // Look for new cards

    if (mfrc522[reader].PICC_IsNewCardPresent() && mfrc522[reader].PICC_ReadCardSerial()) {
      digitalWrite(13, HIGH);
      delay(100);
      digitalWrite(13, LOW);
      Serial.println("------------------CARD DETECTED--------------------");
      Serial.print(F("Reader: "));
      Serial.print(reader);
      // Show some details of the PICC (that is: the tag/card)
      Serial.print(F(" Card UID:"));
      dump_byte_array(mfrc522[reader].uid.uidByte, mfrc522[reader].uid.size);
      Serial.println();
      Serial.print(F("PICC type: "));
      MFRC522::PICC_Type piccType = mfrc522[reader].PICC_GetType(mfrc522[reader].uid.sak);
      Serial.println(mfrc522[reader].PICC_GetTypeName(piccType));

      Serial.println("------------------CARD CHECK--------------------");

      if(memcmp(OurCardUID, mfrc522[reader].uid.uidByte, mfrc522[reader].uid.size) == 0) { //Check the scanned card against our known good cards.
        Serial.println("#######MATCH#######");
        UnlockPC(); //Run the keyboard unlock subroutine.
      }
      else {
        Serial.println("#######FAIL#######");
      }

      Serial.println("------------------CARD CHECK--------------------");

      // Halt PICC
      mfrc522[reader].PICC_HaltA();
      // Stop encryption on PCD
      mfrc522[reader].PCD_StopCrypto1();
    } //if (mfrc522[reader].PICC_IsNewC
  } //for(uint8_t reader

} //------------------------END OF VOID LOOP------------------------

//Helper routine to dump a byte array as hex values to Serial.
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

void UnlockPC() { //This is designed for Windows 10 unlock screens. Remove the printing of "a" for other OS.
  delay(100);
  Keyboard.print("a");
  delay(1000);
  Keyboard.print("password");
  Keyboard.press(KEY_RETURN);
  delay(100);
  Keyboard.releaseAll();
  //delay(2000); //prevent it running several times in a row
}

