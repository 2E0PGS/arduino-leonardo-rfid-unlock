# Arduino Leonardo RFID PC Unlocker #

![IMGDEMORFID.jpg](https://bitbucket.org/repo/gzLaoq/images/1963362415-IMGDEMORFID.jpg)

### What is this repository for? ###

* Arduino Leonardo RFID PC Unlocker.
* Version 1

### How do I get set up? ###

* Clone Repo or Download from https://bitbucket.org/2E0PGS/arduino_leonardo_rfid_unlock/downloads
* Wireup RC522 Module using table found online or in code.
* Download the RC522 Lib. Also requires Keyboard Lib.
* Password is set at the bottom of the code.